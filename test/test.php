<?php

use JohnSear\FileSystem\FileSystem;

$rootPath = rtrim(str_replace(basename(__DIR__),'', __DIR__), DIRECTORY_SEPARATOR);

$vendorPath     = $rootPath . DIRECTORY_SEPARATOR . 'vendor';

/** make composer install in filesystem package root folder first for testing */
require_once $vendorPath . DIRECTORY_SEPARATOR . 'autoload.php';

$fs = new FileSystem();

$testPath       = $rootPath . $fs::DS . 'test';
$testFolderPath = $testPath . $fs::DS . 'testFolder';

$folderContents = $fs->getFolderContents($testFolderPath);
$folderContentsRecursive = $fs->getFolderContentsRecursive($testFolderPath);

echo PHP_EOL;
echo '$folderContents' . PHP_EOL;
echo PHP_EOL;
echo buildFolderContentString($folderContents) . PHP_EOL;

echo PHP_EOL;
echo '------------------' . PHP_EOL;
echo PHP_EOL;

echo PHP_EOL;
echo '$folderContentsRecursive' . PHP_EOL;
echo PHP_EOL;
echo buildFolderContentNestedString($folderContentsRecursive) . PHP_EOL;

function buildFolderContentString(ArrayIterator $folderContents): string
{
    $lastPath = '';
    $return = '';
    foreach ($folderContents as $file) {
        if ($lastPath !== $file->getPath()) {
            $lastPath = $file->getPath();
            $return .= $file->getPath() . ' =>' . PHP_EOL;
        }
        $separator = ($file->getType() === 'file') ? ' ' : '  ';
        $return .= '  (' . $file->getType() . ')' . $separator . $file->getBasename() . PHP_EOL;
    }

    return $return;
}

function buildFolderContentNestedString($a, $lastPath = '', $intend = ''): string
{
    $return = '';

    if ($a instanceof RecursiveArrayIterator) {
        $a = $a->getArrayCopy();
        foreach ($a as $value) {

            if ($value instanceof SplFileInfo) {
                $path = basename($value->getPath());

                if ($lastPath !== $path) {
                    if (strlen($path) > strlen($lastPath)) {
                        $intend .= '  ';
                    } else {
                        $intend = substr($intend, 2);
                    }
                    $lastPath = $path;
                    $return .= substr($intend, 2) . $lastPath . ' =>' . PHP_EOL;
                }
            }
            $return .= buildFolderContentNestedString($value, $lastPath, $intend);
        }
    }

    if ($a instanceof SplFileInfo) {
        $return .= $intend . $a->getFilename() . PHP_EOL;
    }

    return $return;
}
