<?php declare(strict_types=1);

namespace JohnSear\FileSystem;

use ArrayIterator;
use DirectoryIterator;
use FilesystemIterator;
use JohnSear\FileSystem\Exception\FileCreationException;
use JohnSear\FileSystem\Exception\FileNotValidException;
use JohnSear\FileSystem\Exception\FilePathNotFoundException;
use JohnSear\FileSystem\Exception\FilePathNotValidException;
use RecursiveArrayIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use SplFileInfo;

class FileSystem
{
    public const DS = DIRECTORY_SEPARATOR;
    
    public function mayCreateFolderPath(string $folderPath): bool
    {
        $folderPath = trim($folderPath, '/\\');

        $path = ($folderPath !== '') ? self::DS . trim($folderPath, '/\\') . self::DS : '';

        if ($path === '') {
            return false;
        }

        if (!$this->directoryExists($path)) {

            if (!mkdir($path) && !is_dir($path)) {
                return false;
            }

            return $this->changeFileMode($path, 0777);
        }

        return true;
    }

    public function changeFileMode(string $filename, int $mode): bool
    {
        if (!$this->fileExists($filename) || (strlen((string)$mode) < 3 || strlen((string)$mode) > 4)) {
            return false;
        }

        if (strlen((string)$mode) === 3) {
            $modeString = '0' . $mode;
            $mode = (int) $modeString;
        }

        return chmod($filename, $mode);
    }

    public function directoryExists(string $path): bool
    {
        return (is_dir($path));
    }

    public function fileExists(string $file): bool
    {
        return (file_exists($file));
    }

    /**
     * @throws FilePathNotFoundException
     * @throws FileNotValidException
     * @throws FilePathNotValidException
     * @throws FileCreationException
     */
    public function createFile(string $file, string $content = '', $mode = 'ab'): void
    {
        $this->validateFile($file);

        $buildFile = $this->buildFile($file);

        $filePath = $buildFile->getDirname();
        $fileName = $buildFile->getBasename();

        if (! $this->directoryExists($filePath)) {
            throw new FilePathNotValidException('Not a valid directory to create the file.');
        }

        $newFile = fopen($filePath . self::DS . $fileName, $mode);

        if ($newFile === false) {
            throw new FileCreationException('File creation Error: Unable to open file ' . $fileName . '!');
        }

        fwrite($newFile, $content);
        fclose($newFile);
    }

    /**
     * @throws FilePathNotFoundException
     * @throws FileNotValidException
     * @throws FilePathNotValidException
     * @throws FileCreationException
     */
    public function createRotatingFile(string $file, string $content = '', $forceNewFile = false, $maxSize = 1024): void
    {
        $this->createFile($file, $content);

        $buildFile = $this->buildFile($file);

        if($forceNewFile || (($buildFile->getSize() / $maxSize) / $maxSize) > 1.5) {
            $filePath       = $buildFile->getDirname();
            $fileName       = $buildFile->getBasename();
            $singleFileName = $buildFile->getFilename();
            $fileExt        = '.' . $buildFile->getExtension();

            $newFileName = $filePath.$singleFileName.'.'.time().$fileExt;

            @rename($filePath.$fileName, $newFileName);
        }
    }

    /**
     * @throws FilePathNotFoundException
     * @throws FileNotValidException
     */
    public function getFileContents(string $file): string
    {
        $this->validateFile($file);

        $buildFile = $this->buildFile($file);

        $filePath = $buildFile->getDirname();
        $fileName = $buildFile->getBasename();

        return file_get_contents($filePath . self::DS . $fileName);
    }

    public function getFolderContents(string $path, string $pattern = ''): ArrayIterator
    {
        $folderContents = new ArrayIterator();

        $allFiles = new DirectoryIterator($path);

        if ($pattern !== '') {
            $filteredFiles = new RegexIterator($allFiles, $pattern);
            $allFiles = $filteredFiles;
        }

        /** @var SplFileInfo $file */
        foreach ($allFiles as $file) {
            $iteratedFile = clone $file;

            if ($pattern === '' && ($iteratedFile->getFilename() === '.' || $iteratedFile->getFilename() === '..')) {
                continue;
            }

            $folderContents[$iteratedFile->getPath() . DIRECTORY_SEPARATOR . $iteratedFile->getFilename()] = $iteratedFile;
        }

        $folderContents->ksort(SORT_REGULAR);

        $folderContentsFolderFirstThenFiles = new ArrayIterator();
        foreach ($folderContents as $file) {
            if ($this->getFileType($file) === 'dir') {
                $folderContentsFolderFirstThenFiles[$file->getFilename()] = $file;
            }
        }
        foreach ($folderContents as $file) {
            if ($this->getFileType($file) === 'file') {
                $folderContentsFolderFirstThenFiles[$file->getFilename()] = $file;
            }
        }

        return $folderContentsFolderFirstThenFiles;
    }

    public function getFolderContentsRecursive(string $path, string $pattern = ''): RecursiveArrayIterator
    {
        $folderContentsRecursive = new RecursiveArrayIterator();

        if ($pattern === '') {
            $allFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS));
        } else {
            $allFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
            $filteredFiles = new RegexIterator($allFiles, $pattern);
            $allFiles = $filteredFiles;
        }

        /** @var SplFileInfo $file */
        foreach ($allFiles as $path_key => $file) {
            $iteratedFile = clone $file;
            $folderContentsRecursive[$path_key] = $iteratedFile;
        }

        $folderContentsRecursive->ksort(SORT_REGULAR);

        return $folderContentsRecursive;
    }

    /**
     * @throws FilePathNotFoundException
     * @throws FileNotValidException
     */
    public function validateFile(string $file): void
    {
        $fileObject = $this->buildFile($file);

        if ($fileObject->getDirname() === '') {
            throw new FilePathNotFoundException('No File Path given.');
        }

        if ($fileObject->getBasename() === '' || $fileObject->getExtension() === '') {
            throw new FileNotValidException('No valid File given.');
        }
    }

    public function getFileType($file): string
    {
        $type = 'unknown';

        if (is_string($file)) {
            if (file_exists($file)) {
                try {
                    $file = new SplFileInfo($file);
                } catch (\Exception $e) {
                    // don't throw errors here
                    $file = null;
                }
            } else {
                throw new \RuntimeException('File or Directory not found!');
            }
        }
        
        if ($file instanceof SplFileInfo) {
            try {
                $type = $file->getType();
            } catch (\Exception $e) {
                // don't throw errors here
            }
        }
        
        return $type;
    }

    public function buildFile(string $file): File
    {
        $filePathInfo = pathinfo($file);
        $dirname      = (array_key_exists('dirname'  , $filePathInfo)) ? $filePathInfo['dirname']   : '';
        $filename     = (array_key_exists('filename' , $filePathInfo)) ? $filePathInfo['filename']  : '';
        $basename     = (array_key_exists('basename' , $filePathInfo)) ? $filePathInfo['basename']  : '';
        $extension    = (array_key_exists('extension', $filePathInfo)) ? $filePathInfo['extension'] : '';

        return (new File())
            ->setDirname($dirname)
            ->setBasename($basename)
            ->setFilename($filename)
            ->setExtension($extension);
    }
}
