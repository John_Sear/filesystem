<?php declare(strict_types=1);

namespace JohnSear\FileSystem\Exception;

class FileCreationException extends AbstractFileSystemException
{

}
