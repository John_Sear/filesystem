<?php declare(strict_types=1);

namespace JohnSear\FileSystem;

class File
{
    private $dirname;
    private $filename;
    private $basename;
    private $extension;

    public function getDirname(): string
    {
        return (string) $this->dirname;
    }

    public function setDirname(string $dirname): self
    {
        $this->dirname = $dirname;

        return $this;
    }

    public function getFilename(): string
    {
        return (string) $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getBasename(): string
    {
        return (string) $this->basename;
    }

    public function setBasename(string $basename): self
    {
        $this->basename = $basename;

        return $this;
    }

    public function getExtension(): string
    {
        return (string) $this->extension;
    }

    public function setExtension($extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getSize(): int
    {
        return (int) filesize($this->getDirname() . FileSystem::DS .  $this->getBasename());
    }
}
